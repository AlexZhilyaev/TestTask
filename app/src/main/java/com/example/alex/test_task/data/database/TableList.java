package com.example.alex.test_task.data.database;

import com.example.alex.test_task.data.entity.SpecialtyEntity;
import com.yahoo.squidb.sql.Table;

public class TableList {
    public static final Table[] tables = new Table[]{
            SpecialtyEntity.TABLE
    };
}
