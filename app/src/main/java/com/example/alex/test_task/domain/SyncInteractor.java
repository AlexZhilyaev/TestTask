package com.example.alex.test_task.domain;

import com.example.alex.test_task.data.SyncService;
import com.example.alex.test_task.data.dao.SpecialtyEntityDao;
import com.example.alex.test_task.data.entity.SpecialtyEntity;
import com.example.alex.test_task.data.mappers.SpecialtyMapper;
import com.example.alex.test_task.presentation.common.SchedulerPair;

import java.util.List;

import rx.Observable;
import rx.functions.Func1;

public class SyncInteractor extends Intercator<Object, Boolean> {

    private SyncService syncService;
    private SpecialtyEntityDao specialtyEntityDao;
    private SpecialtyMapper specialtyMapper;

    public SyncInteractor(SchedulerPair schedulerPair) {
        super(schedulerPair);
        syncService = new SyncService();
        specialtyEntityDao = new SpecialtyEntityDao();
        specialtyMapper = new SpecialtyMapper();
    }

    @Override
    protected Observable<Boolean> createObservable(Object requestModel) {
        return syncService.sync()
                .map(specialtyMapper)
                .map(new Func1<List<SpecialtyEntity>, Boolean>() {
                    @Override
                    public Boolean call(List<SpecialtyEntity> specialtyEntities) {
                        specialtyEntityDao.persistAll(specialtyEntities);
                        return true;
                    }
                });
    }
}
