package com.example.alex.test_task.data.response;

import java.util.List;

public class DataModel {
    public List<Response> response;

    public class Response {
        public String f_name;
        public String l_name;
        public String birthday;
        public String avatr_url;
        public List<Specialty> specialty;
    }

    public class Specialty {
        public String specialty_id;
        public String name;
    }
}
