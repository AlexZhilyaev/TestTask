package com.example.alex.test_task.presentation.common;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.example.alex.test_task.data.database.SquiDBComponent;
import com.example.alex.test_task.presentation.common.dagger.DaggerNetComponent;
import com.example.alex.test_task.presentation.common.dagger.NetComponent;
import com.example.alex.test_task.presentation.common.dagger.NetModule;
import com.yahoo.squidb.android.AndroidOpenHelper;
import com.yahoo.squidb.data.ISQLiteOpenHelper;
import com.yahoo.squidb.data.SquidDatabase;

import java.io.File;

public class Application extends android.app.Application {

    private static NetComponent netComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        netComponent = DaggerNetComponent.builder()
                .netModule(new NetModule("http://gitlab.65apps.com/65gb/static/raw/master/"))
                .build();
        coreInt(this);
    }

    private void coreInt(final Context context) {
        CoreComponents.getInstance().setSqLightComponent(new SquiDBComponent() {
            @Override
            public ISQLiteOpenHelper createOpenHelper(String databaseName, SquidDatabase.OpenHelperDelegate delegate, int version) {
                return new AndroidOpenHelper(context, databaseName, delegate, version) {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onConfigure(SQLiteDatabase db) {
                        db.setForeignKeyConstraintsEnabled(true);
                    }
                };
            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void deleteDatabase(String path) {
                SQLiteDatabase.deleteDatabase(new File(path));
            }
        });
    }

    public static NetComponent getNetComponent() {
        return netComponent;
    }
}
