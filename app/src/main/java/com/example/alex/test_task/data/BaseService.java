package com.example.alex.test_task.data;

import com.example.alex.test_task.presentation.common.Application;

public class BaseService<THttpService> {

    protected THttpService httpService;
    private Class<THttpService> clazz;

    public THttpService getHttpService() {
        return httpService;
    }

    protected BaseService(Class<THttpService> typeClass) {
        httpService = Application.getNetComponent().getRetrofit().create(typeClass);
        clazz = typeClass;
    }
}
