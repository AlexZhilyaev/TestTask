package com.example.alex.test_task.data.dao;

import com.example.alex.test_task.data.database.CoreDB;
import com.yahoo.squidb.data.SquidDatabase;
import com.yahoo.squidb.data.TableModel;
import com.yahoo.squidb.sql.TableStatement;

import java.util.Collection;

public class BaseEntityDao<TEntity extends TableModel> {

    private SquidDatabase mDatabase;
    protected Class<TEntity> typeClass;

    BaseEntityDao(Class<TEntity> typeClass) {
        this.typeClass = typeClass;
        this.mDatabase = CoreDB.getInstance();
    }

    public boolean persist(TEntity entity) {
        return entity != null
                && mDatabase.persistWithOnConflict(entity, TableStatement.ConflictAlgorithm.REPLACE);
    }

    public final void persistAll(Collection<TEntity> entities) {
        for (TEntity e : entities) {
            this.persist(e);
        }
    }
}
