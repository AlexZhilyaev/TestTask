package com.example.alex.test_task.presentation.common;

import rx.Scheduler;

public class SchedulerPair {
    public Scheduler subscribeOn;
    public Scheduler observeOn;

    public SchedulerPair(Scheduler subscribeOn, Scheduler observeOn) {
        this.observeOn = observeOn;
        this.subscribeOn = subscribeOn;
    }
}
