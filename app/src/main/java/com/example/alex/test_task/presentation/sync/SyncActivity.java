package com.example.alex.test_task.presentation.sync;

import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.alex.test_task.R;

public class SyncActivity extends MvpAppCompatActivity implements SyncView {

    @InjectPresenter
    SyncPresenter syncPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sunc);
    }
}
