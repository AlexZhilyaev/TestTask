package com.example.alex.test_task.data;

import com.example.alex.test_task.data.response.DataModel;

import rx.Observable;

public class SyncService extends BaseService<ISyncService> {
    public SyncService() {
        super(ISyncService.class);
    }

    public Observable<DataModel> sync() {
        return httpService.sync();
    }

}
