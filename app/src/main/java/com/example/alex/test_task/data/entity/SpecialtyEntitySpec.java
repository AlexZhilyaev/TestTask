package com.example.alex.test_task.data.entity;

import com.yahoo.squidb.annotations.PrimaryKey;
import com.yahoo.squidb.annotations.TableModelSpec;

@TableModelSpec(className = "SpecialtyEntity", tableName = "specialty")
public class SpecialtyEntitySpec {

    @PrimaryKey
    long specialty_id;

    String specialty_name;
}
