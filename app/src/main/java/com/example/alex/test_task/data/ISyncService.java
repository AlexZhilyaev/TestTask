package com.example.alex.test_task.data;


import com.example.alex.test_task.data.response.DataModel;

import retrofit2.http.GET;
import rx.Observable;

public interface ISyncService {

    @GET("testTask.json")
    Observable<DataModel> sync();
}
