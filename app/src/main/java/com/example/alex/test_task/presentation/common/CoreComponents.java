package com.example.alex.test_task.presentation.common;

import com.example.alex.test_task.data.database.SquiDBComponent;

public class CoreComponents {
    private static CoreComponents instance;

    public static CoreComponents getInstance() {
        if (instance == null) {
            instance = new CoreComponents();
        }
        return instance;
    }

    private CoreComponents() {
    }

    private SquiDBComponent sqLightComponent;

    public SquiDBComponent getSqLightComponent() {
        return sqLightComponent;
    }

    public void setSqLightComponent(SquiDBComponent sqLightComponent) {
        this.sqLightComponent = sqLightComponent;
    }


}
