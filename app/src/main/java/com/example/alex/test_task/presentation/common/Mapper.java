package com.example.alex.test_task.presentation.common;

import rx.exceptions.Exceptions;
import rx.functions.Func1;

public abstract class Mapper<From, To> implements Func1<From, To> {

    protected abstract To map(From entity) throws Exception;

    @Override
    public final To call(From from) {
        try {
            return map(from);
        } catch (Exception e) {
            throw Exceptions.propagate(e);
        }
    }
}