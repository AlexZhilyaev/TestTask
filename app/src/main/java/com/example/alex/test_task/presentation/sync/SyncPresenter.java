package com.example.alex.test_task.presentation.sync;

import com.arellomobile.mvp.InjectViewState;
import com.example.alex.test_task.domain.SyncInteractor;
import com.example.alex.test_task.presentation.common.BasePresenter;
import com.example.alex.test_task.presentation.common.SchedulerPair;

import rx.Subscriber;
import rx.schedulers.Schedulers;

@InjectViewState
public class SyncPresenter extends BasePresenter<SyncView> {

    private SyncInteractor syncInteractor;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        syncInteractor = new SyncInteractor(new SchedulerPair(Schedulers.io(), Schedulers.io()));
        unsubscribeOnDestroy(syncInteractor);
        syncInteractor.execute(new Subscriber<Boolean>() {
            @Override
            public void onCompleted() {
                int a = 0;
            }

            @Override
            public void onError(Throwable e) {
                int a = 0;
            }

            @Override
            public void onNext(Boolean model) {
                int a = 0;
            }
        }, null);
    }
}
