package com.example.alex.test_task.domain;

import com.example.alex.test_task.presentation.common.SchedulerPair;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

abstract class Intercator<Request, Response> implements Subscription {

    private Scheduler subscribeOn;
    private Scheduler observeOn;
    private CompositeSubscription subscription = new CompositeSubscription();

    public Intercator(SchedulerPair schedulerPair) {
        subscribeOn = schedulerPair.subscribeOn;
        observeOn = schedulerPair.observeOn;
    }

    public void execute(Subscriber<Response> subscriber, Request request) {
        subscription.add(createObservable(request)
                .subscribeOn(subscribeOn)
                .observeOn(observeOn)
                .subscribe(subscriber));
    }

    @Override
    public void unsubscribe() {
        subscription.clear();
    }

    @Override
    public boolean isUnsubscribed() {
        return !subscription.hasSubscriptions();
    }

    protected abstract Observable<Response> createObservable(Request requestModel);
}
