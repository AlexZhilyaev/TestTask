package com.example.alex.test_task.data.dao;

import com.example.alex.test_task.data.entity.SpecialtyEntity;

public class SpecialtyEntityDao extends BaseEntityDao {
    public SpecialtyEntityDao() {
        super(SpecialtyEntity.class);
    }
}
