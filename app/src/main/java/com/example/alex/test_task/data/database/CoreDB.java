package com.example.alex.test_task.data.database;

import com.example.alex.test_task.presentation.common.CoreComponents;
import com.yahoo.squidb.data.ISQLiteDatabase;
import com.yahoo.squidb.data.ISQLiteOpenHelper;
import com.yahoo.squidb.data.SquidDatabase;
import com.yahoo.squidb.sql.Table;

public class CoreDB extends SquidDatabase {

    private static CoreDB instance = null;

    @Override
    public String getName() {
        return "test_task.db";
    }

    public static CoreDB getInstance() {
        if (instance == null) {
            instance = new CoreDB();
        }
        return instance;
    }

    @Override
    protected int getVersion() {
        return 1;
    }

    @Override
    protected Table[] getTables() {
        return TableList.tables;
    }

    @Override
    protected boolean onUpgrade(ISQLiteDatabase db, int oldVersion, int newVersion) {
        return false;
    }

    @Override
    protected ISQLiteOpenHelper createOpenHelper(String databaseName, OpenHelperDelegate delegate, int version) {
        return CoreComponents.getInstance().getSqLightComponent().createOpenHelper(databaseName, delegate, version);
    }
}
