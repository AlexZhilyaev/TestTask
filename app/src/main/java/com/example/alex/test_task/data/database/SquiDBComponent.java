package com.example.alex.test_task.data.database;

import com.yahoo.squidb.data.ISQLiteOpenHelper;
import com.yahoo.squidb.data.SquidDatabase;

public abstract class SquiDBComponent {
    public abstract ISQLiteOpenHelper createOpenHelper(String databaseName,
                                                       SquidDatabase.OpenHelperDelegate delegate, int version);

    public abstract void deleteDatabase(String path);
}
