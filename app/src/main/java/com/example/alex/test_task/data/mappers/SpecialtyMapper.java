package com.example.alex.test_task.data.mappers;

import com.example.alex.test_task.data.entity.SpecialtyEntity;
import com.example.alex.test_task.data.response.DataModel;
import com.example.alex.test_task.presentation.common.Mapper;

import java.util.ArrayList;
import java.util.List;

public class SpecialtyMapper extends Mapper<DataModel, List<SpecialtyEntity>> {
    @Override
    protected List<SpecialtyEntity> map(DataModel entity) throws Exception {
        List<SpecialtyEntity> specialtyEntities = new ArrayList<>();
        for (DataModel.Response response : entity.response) {
            for (DataModel.Specialty specialty : response.specialty) {
                SpecialtyEntity specialtyEntity = new SpecialtyEntity();
                specialtyEntity.setSpecialtyId(Long.valueOf(specialty.specialty_id));
                specialtyEntity.setSpecialtyName(specialty.name);
                specialtyEntities.add(specialtyEntity);
            }
        }
        return specialtyEntities;
    }
}
