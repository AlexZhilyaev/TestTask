package com.example.alex.test_task.presentation.common.dagger;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {NetModule.class})
public interface NetComponent {
    Retrofit getRetrofit();
}
